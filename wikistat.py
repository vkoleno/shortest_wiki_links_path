import re
import os
from bs4 import BeautifulSoup
from bs4.element import Tag


def build_tree(path):
    link_re = re.compile(r"(?<=/wiki/)[\w()]+")
    files = dict.fromkeys(os.listdir(path))
    adj_list = dict()
    for file in files:
        with open(os.path.join(path, file), encoding='utf_8_sig') as f:
            neighbors = []
            page_content = f.read()
            soup = BeautifulSoup(page_content, 'html.parser')
            topic_body = soup.find('div', id='bodyContent')
            hrefs = [tag['href'] if '/' in tag['href']  else '' for tag in topic_body.find_all('a', href=True)]
            hrefs_endings = list(map(lambda x: x.split('/')[-1], hrefs))
            hrefs_cleaned = set(list(filter(lambda x: '.' not in x and ':' not in x and x in files, hrefs_endings)))
            adj_list[file] = hrefs_cleaned
    return adj_list

def build_bridge(start, end, path, adj_list):
    visit_marks = {file: False for file, _ in dict.fromkeys(os.listdir(path)).items()}
    queue = [[start, True]]
    ancestors = dict()
    while len(queue) > 0:
        current_node = queue[0][0]
        neighbors = adj_list[current_node]
        for neighbor in neighbors:
            if visit_marks[neighbor] == False:
                queue.append([neighbor, True])
                visit_marks[neighbor] = True
                ancestors[neighbor] = current_node
        del queue[0]
        
    is_path_found = False
    short_path = [end]
    curr_node = ancestors[end]
    while not is_path_found:
        short_path.append(curr_node)
        if ancestors[curr_node] != start:
            curr_node = ancestors[curr_node]
        else:
            is_path_found = True
            short_path.append(start)
    return short_path

def parse(start, end, path):
    adj_list = build_tree(path)
    short_path = build_bridge(start, end, path, adj_list)
    out = {}
    for file in short_path:
        with open(os.path.join(path, file), encoding='utf_8_sig') as data:
            soup = BeautifulSoup(data, "lxml")
            body = soup.find(id="bodyContent")

            imgs = body.find_all('img')
            num_imgs = [img.get('width', 0) for img in body.find_all('img')]
            num_imgs = len([size for size in num_imgs if int(size) >=200])
                
            headers = body.find_all(re.compile('^h[1-6]$'))
            num_h = len([header.text for header in headers if header.text.startswith(('C', 'E', 'T'))])

            a_list = body.find_all('a')
            length_list = []
            for a in a_list:
                max_a_length = 1
                for sibling in a.next_siblings:
                    if type(sibling) != Tag:
                        continue
                    elif sibling.name == 'a':
                        max_a_length += 1
                    else:
                        length_list.append(max_a_length)
                        break
            linkslen = max(length_list)

            lists = body.find_all(('ol', 'ul'))
            num_lists = len([list_ for list_ in lists if list_.find_parents(('ol', 'ul')) == []])

            out[file] = [num_imgs, num_h, linkslen, num_lists]

    return out
